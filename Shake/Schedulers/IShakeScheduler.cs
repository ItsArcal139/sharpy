﻿using ShakeAPI.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using ShakeAPI;
using ShakeAPI.Util;

namespace ShakeAPI.Schedulers
{
    public interface IShakeScheduler
    {
        int ScheduleSyncDelayedTask(IPlugin plugin, Action task, long delay);
        [Obsolete]
        int ScheduleSyncDelayedTask(IPlugin plugin, ShakeRunnable task, long delay);
        [Obsolete]
        int ScheduleSyncDelayedTask(IPlugin plugin, IRunnable task, long delay);

        IShakeTask RunTask(IPlugin plugin, Action task);
        [Obsolete]
        IShakeTask RunTask(IPlugin plugin, IRunnable task);
        [Obsolete]
        IShakeTask RunTask(IPlugin plugin, ShakeRunnable task);

        void CancelTask(int taskId);
        void CancelTasks(IPlugin plugin);
    }

    public abstract class ShakeRunnable : IRunnable
    {
        private IShakeTask task;

        public abstract void Run();

        public int TaskID
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                CheckScheduled();
                return task.TaskID;
            }
        }

        public bool IsCancelled
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                CheckScheduled();
                return task.IsCancelled;
            }
        }

        public void Cancel()
        {
            Shake.Scheduler.CancelTask(TaskID);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IShakeTask RunTask(IPlugin plugin)
        {
            CheckNotYetScheduled();
            return SetupTask(Shake.Scheduler.RunTask(plugin, Run));
        }

        private void CheckScheduled()
        {
            if(task == null)
            {
                throw new InvalidOperationException("Not scheduled yet.");
            }
        }

        private void CheckNotYetScheduled()
        {
            if(task != null)
            {
                throw new InvalidOperationException($"Already scheduled as {task.TaskID}.");
            }
        }

        private IShakeTask SetupTask(IShakeTask task)
        {
            this.task = task;
            return task;
        }
    }

    public interface IShakeTask
    {
        int TaskID { get; }
        IPlugin Owner { get; }
        bool IsSync { get; }
        bool IsCancelled { get; }
        void Cancel();
    }
}
