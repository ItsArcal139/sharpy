﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI
{
    public interface ITag<T> : IKeyed where T : IKeyed
    {
        bool IsTagged(T item);
        ICollection<T> GetValues();
    }

    public static class TagHelper
    {
        public static readonly string RegistryBlocks = "blocks";
        public static readonly ITag<Material> Wool = Shake.GetTag<Material>(RegistryBlocks, NamespacedKey.MinecraftKey("wool"), typeof(Material));
        // TODO: more...
    }
}
