﻿using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ShakeAPI.Configuration.Serialization
{
    public class ConfigurationSerialization
    {
        public const string SERIALIZED_TYPE_KEY = "==";
        private Type clazz;
        private static Dictionary<string, Type> aliases = new Dictionary<string, Type>();

        static ConfigurationSerialization()
        {
            // TODO: more...
            RegisterClass(typeof(Location));
        }

        protected ConfigurationSerialization(Type type)
        {
            Validate.IsTrue(type.Implements(typeof(IConfigurationSerializable)), "The type must implement IConfigurationSerializable.");
            clazz = type;
        }

        protected MethodInfo GetMethod(string name, bool isStatic)
        {
            try
            {
                MethodInfo method = clazz.GetMethod(name, new Type[] { typeof(Dictionary<string, dynamic>) });

                if (!typeof(IConfigurationSerializable).IsAssignableFrom(method.ReturnType))
                {
                    return null;
                }

                if (method.IsStatic != isStatic)
                {
                    return null;
                }

                return method;
            }
            catch (AmbiguousMatchException)
            {
                return null;
            }
            catch (ArgumentException)
            {
                return null;
            }
        }

        protected ConstructorInfo GetConstructor()
        {
            try
            {
                return clazz.GetConstructor(new Type[] { typeof(Dictionary<string, dynamic>) });
            }
            catch (ArgumentException)
            {
                return null;
            }
        }

        protected IConfigurationSerializable DeserializeViaMethod(MethodInfo method, Dictionary<string, dynamic> args)
        {
            try
            {
                IConfigurationSerializable result = (IConfigurationSerializable)method.Invoke(null, new object[] { args });

                if (result == null)
                {
                    Logger.GetLogger(typeof(IConfigurationSerializable).Name).Log(Level.Severe, string.Format("Could not call method {0} of {1} for deserialization: method returned null.", method.ToString(), clazz.ToString()));
                }
                else
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.GetLogger(typeof(IConfigurationSerializable).Name)
                    .Log(Level.Severe, string.Format("Could not call method {0} of {1} for deserialization: {2}", method.ToString(), clazz.ToString(), ex.ToString()));
            }

            return null;
        }

        protected IConfigurationSerializable DeserializeViaCtor(ConstructorInfo ctor, Dictionary<string, dynamic> args)
        {
            try
            {
                return (IConfigurationSerializable)ctor.Invoke(new object[] { args });
            }
            catch (Exception ex)
            {
                Logger.GetLogger(typeof(IConfigurationSerializable).Name)
                    .Log(Level.Severe, string.Format("Could not call constructor {0} of {1} for deserialization: {2}", ctor.ToString(), clazz.ToString(), ex.ToString()));
            }

            return null;
        }

        public IConfigurationSerializable Deserialize(Dictionary<string, dynamic> args)
        {
            Validate.NotNull(args, "Args must not be null.");

            IConfigurationSerializable result = null;
            MethodInfo method = null;

            if (result == null)
            {
                method = GetMethod("Deserialize", true);

                if (method != null)
                {
                    result = DeserializeViaMethod(method, args);
                }
            }

            if (result == null)
            {
                method = GetMethod("valueOf", true);

                if (method != null)
                {
                    result = DeserializeViaMethod(method, args);
                }
            }

            if (result == null)
            {
                ConstructorInfo ctor = GetConstructor();
                if (ctor != null)
                {
                    result = DeserializeViaCtor(ctor, args);
                }
            }

            return result;
        }

        public static IConfigurationSerializable DeserializeObject(Dictionary<string, dynamic> args, Type clazz)
        {
            return new ConfigurationSerialization(clazz).Deserialize(args);
        }

        public static IConfigurationSerializable DeserializeObject(Dictionary<string, dynamic> args)
        {
            Type clazz = null;

            if (args.ContainsKey(SERIALIZED_TYPE_KEY))
            {
                try
                {
                    string alias = (string)args[SERIALIZED_TYPE_KEY];

                    if (alias == null)
                    {
                        throw new ArgumentException("Cannot have null alias.");
                    }

                    clazz = GetClassByAlias(alias);

                    if (clazz == null)
                    {
                        throw new ArgumentException(string.Format("Specified class does not exist ('{0}')", alias));
                    }
                }
                catch (InvalidCastException ex)
                {
                    throw new InvalidCastException("Invalid cast to string!", ex);
                }
            }
            else
            {
                throw new ArgumentException(string.Format("Args doesn't contain type key ('{0}').", SERIALIZED_TYPE_KEY));
            }

            return new ConfigurationSerialization(clazz).Deserialize(args);
        }

        public static void RegisterClass(GenericType<IConfigurationSerializable> clazz)
        {
            DelegateDeserialization @delegate = clazz.Type.GetCustomAttribute<DelegateDeserialization>();

            if (@delegate == null)
            {
                RegisterClass(clazz, GetAlias(clazz));
                RegisterClass(clazz, clazz.Type.Name);
            }
        }

        public static void RegisterClass(GenericType<IConfigurationSerializable> clazz, string alias)
        {
            aliases.Add(alias, clazz.Type);
        }

        public static void UnregisterClass(string alias)
        {
            aliases.Remove(alias);
        }

        public static void UnregisterClass(GenericType<IConfigurationSerializable> clazz)
        {
            foreach (KeyValuePair<string, Type> item in aliases.SkipWhile(item => !item.Value.Equals(clazz.Type)))
            {
                aliases.Remove(item.Key);
            }
        }

        public static Type GetClassByAlias(string alias)
        {
            return aliases[alias];
        }

        public static string GetAlias(GenericType<IConfigurationSerializable> clazz)
        {
            DelegateDeserialization @delegate = clazz.Type.GetCustomAttribute<DelegateDeserialization>();

            if (@delegate != null)
            {
                if ((@delegate.Value == null) || (@delegate.Value == clazz.Type))
                {
                    @delegate = null;
                }
                else
                {
                    return GetAlias(@delegate.Value);
                }
            }

            if (@delegate == null)
            {
                SerializableAs alias = clazz.Type.GetCustomAttribute<SerializableAs>();

                if ((alias != null) && (alias.Value != null))
                {
                    return alias.Value;
                }
            }

            return clazz.Type.Name;
        }
    }
}
