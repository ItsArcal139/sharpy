﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Configuration.Serialization
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SerializableAs : Attribute
    {
        public string Value { get; private set; }

        public SerializableAs(string value)
        {
            Value = value;
        }
    }
}
