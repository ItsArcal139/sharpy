﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Configuration.Serialization
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DelegateDeserialization : Attribute
    {
        public Type Value { get; private set; }

        public DelegateDeserialization(Type value)
        {
            Value = value;
        }
    }
}
