﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI
{
    /// <summary>
    ///     <para>
    ///         This annotation indicates a method (and sometimes constructor) will chain
    ///         its internal operations.
    ///     </para>
    ///     <para>
    ///         This is solely meant for identifying methods that don't need to be
    ///         overridden / handled manually.
    ///     </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method)]
    public class UtilityAttribute : Attribute
    {
        // ;
    }
}
