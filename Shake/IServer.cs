﻿using System;
using System.Collections.Generic;
using ShakeAPI.Entity;
using ShakeAPI.Plugins;
using ShakeAPI.Schedulers;
using ShakeAPI.Util;

namespace ShakeAPI
{
    public interface IServer
    {
        Logger Logger { get; }
        string Name { get; }
        string Version { get; }
        string ShakeVersion { get; }
        ICollection<T> GetOnlinePlayers<T>() where T : IPlayer;
        int MaxPlayers { get; }
        int Port { get; }
        int ViewDistance { get; }
        string IP { get; }
        string ServerName { get; }
        string ServerID { get; }
        string WorldType { get; }
        bool GenerateStructures { get; }
        bool AllowEnd { get; }
        bool AllowNether { get; }
        bool HasWhitelist { get; set; }
        ICollection<IOfflinePlayer> GetWhitelistedPlayers();
        void ReloadWhitelist();
        int BroadcastMessage(string message);
        string UpdateFolder { get; }
        // File UpdateFolderFile { get; }
        long ConnectionThrottle { get; }
        int TicksPerAnimalSpawns { get; }
        int TicksPerMonsterSpawns { get; }
        [Obsolete]
        IPlayer GetPlayer(string name);
        [Obsolete]
        IPlayer GetPlayerExact(string name);
        [Obsolete]
        List<IPlayer> MatchPlayer(string name);
        IPlayer GetPlayer(Uuid id);
        IPluginManager PluginManager { get; }
        IShakeScheduler Scheduler { get; }

        ITag<T> GetTag<T>(string registry, NamespacedKey key, GenericType<T> clazz) where T : IKeyed;
        ICollection<ITag<T>> GetTags<T>(string registry, GenericType<T> clazz) where T : IKeyed;
    }
}