﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Util
{
    public static class Reflection
    {
        public static bool Inherits(this Type type, Type super)
        {
            if(type == null || super == null)
            {
                throw new ArgumentNullException("One of given types is null.");
            }

            if (super.IsInterface)
            {
                throw new ArgumentException("The second argument must not be an interface type.");
            }

            bool result = false;
            Type t = type.BaseType;
            while(t != null && !result)
            {
                result = type.Equals(super);
                if(!result)
                {
                    t = t.BaseType;
                }
            }
            return result;
        }

        public static bool Implements(this Type type, Type itf)
        {
            if (type == null || itf == null)
            {
                throw new ArgumentNullException("One of given types is null.");
            }

            if(!itf.IsInterface)
            {
                throw new ArgumentException("The second argument must be an interface type.");
            }

            bool result = false;
            Type[] ts = type.GetInterfaces();
            foreach(Type t in ts)
            {
                if (t.Equals(itf))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public static bool IsExtendOf(this Type type, Type super)
        {
            if (type == null || super == null)
            {
                throw new ArgumentNullException("One of given types is null.");
            }

            if (type.Equals(super)) return true;
            return type.Inherits(super);
        }
    }

    public class NoSuchMethodException : Exception
    {
        public NoSuchMethodException() : base() { }
        public NoSuchMethodException(string message) : base(message) { }
    }
}
