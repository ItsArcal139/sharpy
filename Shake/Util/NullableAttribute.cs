﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Util
{
    public class NullableAttribute : Attribute
    {
    }

    public class NotNullAttribute : Attribute
    {
    }
}
