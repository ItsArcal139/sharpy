﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Util
{
    public static class StringConvert
    {
        public static string ConvertToUnderlineCase(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach(char c in str)
            {
                if(char.IsUpper(c))
                {
                    sb.Append("_");
                }
                sb.Append(char.ToLower(c));
            }

            return sb.ToString().TrimStart('_');
        }
    }
}
