﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Util
{
    public class Logger
    {
        public Logger Parent { get; set; }
        public string Name { get; private set; }

        public Logger(string name, string resourceBundleName)
        {

        }

        public static Logger GetLogger(string name, string resourceBundleName = "")
        {
            return new Logger(name, resourceBundleName);
        }

        public void Config(string message)
        {

        }

        public void Fine(string message)
        {

        }

        public void Finest(string message)
        {

        }

        public void Info(string message)
        {

        }

        public void Severe(string message)
        {

        }

        public void Warning(string message)
        {

        }

        public void Log(Level level, string str)
        {

        }
    }

    public enum Level
    {
        Severe
    }
}
