﻿using System;

namespace ShakeAPI.Util
{
    public class Validate
    {
        private static void ThrowIf<T>(bool condition, string message) where T : Exception
        {
            if (condition)
            {
                throw (T)typeof(T).GetConstructor(new Type[] { typeof(string) }).Invoke(new object[] { message });
            }
        }

        public static void NotNull(object arg, string message)
        {
            AssertNull<NullReferenceException>(arg, message);
        }

        public static void AssertNull<T>(object arg, string message) where T : Exception
        {
            ThrowIf<T>(arg == null, message);
        }

        public static void Assert<T>(bool condition, string message) where T: Exception
        {
            ThrowIf<T>(!condition, message);
        }

        public static void IsTrue(bool condition, string message)
        {
            Assert<ArgumentException>(!condition, message);
        }
    }
}
