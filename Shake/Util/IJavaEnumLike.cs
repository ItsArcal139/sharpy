﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Util
{
    public interface IJavaEnumLike<T>
    {
        int Ordinal { get; }
    }
}
