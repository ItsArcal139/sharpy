﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Util
{
    public class Uuid
    {
        private byte[] arr;

        public Uuid(byte[] arr)
        {
            this.arr = arr;
        }

        public static Uuid FromGuid(Guid guid)
        {
            byte[] arr = guid.ToByteArray();
            Array.Reverse(arr, 0, 4);
            Array.Reverse(arr, 4, 2);
            Array.Reverse(arr, 6, 2);
            return new Uuid(arr);
        }

        public Guid ToGuid()
        {
            byte[] arr = this.arr.ToArray();
            Array.Reverse(arr, 0, 4);
            Array.Reverse(arr, 4, 2);
            Array.Reverse(arr, 6, 2);
            return new Guid(arr);
        }

        public static Uuid RandomUuid()
        {
            return FromGuid(Guid.NewGuid());
        }
    }
}
