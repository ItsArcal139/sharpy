﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Util
{
    public class GenericType<T>
    {
        public Type Type { get; private set; }

        public GenericType(Type type)
        {
            Validate.IsTrue(typeof(T) == type, $"The type must be {typeof(T).Name}, found {type.Name}.");
        }

        public static implicit operator Type(GenericType<T> t) => t.Type;

        public static implicit operator GenericType<T>(Type t) => new GenericType<T>(t);
    }
}
