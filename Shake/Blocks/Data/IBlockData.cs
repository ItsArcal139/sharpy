﻿using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks.Data
{
    public interface IBlockData : ICloneableG<IBlockData>
    {
        /// <summary>
        /// 這個 <see cref="IBlockData"/> 所代表的方塊類型。
        /// </summary>
        Material Material { get; set; }

        string GetAsString(bool hideUnspecified = false);

        IBlockData Merge(IBlockData data);

        bool Matches(IBlockData data);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        new IBlockData Clone();
    }
}
