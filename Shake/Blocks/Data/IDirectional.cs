﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks.Data
{
    public interface IDirectional : IBlockData
    {
        /// <summary>
        /// 這個方塊面對的方向。
        /// </summary>
        BlockFace Facing { get; set; }

        /// <summary>
        /// 取得可套用至這個方塊的方塊方向。
        /// </summary>
        /// <returns>傳回可套用至這個方塊的方塊方向清單。</returns>
        List<BlockFace> GetFaces();
    }

    public interface IOrientable : IBlockData
    {
        Axis Axis { get; set; }
        List<Axis> GetAxes();
    }
}
