﻿using System.Collections.Generic;

namespace ShakeAPI.Blocks.Data
{
    public interface IMultipleFacing : IBlockData
    {
        bool HasFace(BlockFace face);
        void SetFace(BlockFace face, bool has);
        List<BlockFace> GetFaces();
        List<BlockFace> GetAllowedFaces();
    }
}
