﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks.Data.Types
{
    public interface ISwitch : IDirectional, IPowerable
    {
        SwitchHelper.Face Face { get; set; }
    }

    public static class SwitchHelper
    {
        public enum Face
        {
            Floor, Wall, Ceiling
        }
    }
}
