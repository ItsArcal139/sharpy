﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface IFence : IMultipleFacing, IWaterlogged { }
}
