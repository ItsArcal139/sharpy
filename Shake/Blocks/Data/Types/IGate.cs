﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface IGate : IDirectional, IOpenable, IPowerable
    {
        bool InWall { get; set; }
    }


}
