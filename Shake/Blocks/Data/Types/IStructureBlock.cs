﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface IStructureBlock
    {
        StructureBlockHelper.Mode Mode { get; set; }
    }

    public static class StructureBlockHelper
    {
        public enum Mode
        {
            Save, Load, Corner, Data
        }
    }
}
