﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface IStairs : IBisected, IDirectional, IWaterlogged
    {
        StairsHelper.Shape Shape { get; set; }
    }

    public static class StairsHelper
    {
        public enum Shape
        {
            Straight, InnerLeft, InnerRight, OuterLeft, OuterRight
        }
    }
}
