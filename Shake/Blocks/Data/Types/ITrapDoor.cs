﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface ITrapDoor : IBisected, IDirectional, IOpenable, IPowerable, IWaterlogged
    {
    }
}
