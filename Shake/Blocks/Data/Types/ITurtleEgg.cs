﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface ITurtleEgg : IBlockData
    {
        int Eggs { get; set; }
        int GetMinimumEggs();
        int GetMaximumEggs();
        int Hatch { get; set; }
        int GetMaximumHatch();
    }
}
