﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface ISlab : IWaterlogged
    {
        SlabHelper.Type Type { get; set; }
    }

    public static class SlabHelper
    {
        public enum Type
        {
            Top, Bottom, Double
        }
    }
}
