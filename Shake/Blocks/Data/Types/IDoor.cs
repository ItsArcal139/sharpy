﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks.Data.Types
{
    public interface IDoor : IBisected, IDirectional, IOpenable, IPowerable
    {
        DoorHelper.Hinge Hinge { get; set; }
    }

    public static class DoorHelper
    {
        public enum Hinge
        {
            Left, Right
        }
    }
}
