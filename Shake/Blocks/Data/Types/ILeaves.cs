﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface ILeaves : IBlockData
    {
        bool Persistent { get; set; }
        int Distance { get; set; }
    }

    public interface ISapling : IBlockData
    {
        int Stage { get; set; }
        int GetMaximumStage();
    }
}
