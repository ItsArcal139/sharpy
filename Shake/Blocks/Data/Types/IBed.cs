﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface IBed : IDirectional
    {
        BedHelper.Part Part { get; set; }

        bool Occupied { get; }
    }

    public static class BedHelper
    {
        public enum Part
        {
            Head, Foot
        }
    }
}
