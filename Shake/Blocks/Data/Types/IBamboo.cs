﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface IBamboo : IAgeable, ISapling
    {
        BambooHelper.Leaves Leaves { get; set; }
    }

    public static class BambooHelper
    {
        public enum Leaves
        {
            None, Small, Large
        }
    }
}
