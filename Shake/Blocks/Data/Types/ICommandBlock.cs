﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks.Data.Types
{
    public interface ICommandBlock : IDirectional
    {
        bool Conditional { get; set; }
    }
}
