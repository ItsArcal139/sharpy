﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface IBell : IDirectional
    {
        BellHelper.Attachment Attachment { get; set; }
    }

    public static class BellHelper
    {
        public enum Attachment
        {
            Floor, Ceiling, SingleWall, DoubleWall
        }
    }
}
