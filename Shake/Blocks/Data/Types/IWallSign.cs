﻿namespace ShakeAPI.Blocks.Data.Types
{
    public interface IWallSign : IDirectional, IWaterlogged
    {
    }
}
