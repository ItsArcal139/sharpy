﻿namespace ShakeAPI.Blocks.Data
{
    public interface IRotatable : IBlockData
    {
        BlockFace Rotation { get; set; }
    }
}
