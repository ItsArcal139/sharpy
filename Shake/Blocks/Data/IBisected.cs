﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks.Data
{
    public interface IBisected : IBlockData
    {
        BisectedHelper.Half Half { get; set; }
    }

    public static class BisectedHelper
    {
        public enum Half
        {
            Top, Bottom
        }
    }
}
