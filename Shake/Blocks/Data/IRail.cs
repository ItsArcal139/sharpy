﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks.Data
{
    public interface IRail : IBlockData
    {
        RailHelper.Shape Shape { get; set; }
        ICollection<RailHelper.Shape> GetShapes();
    }

    public static class RailHelper
    {
        public enum Shape
        {
            NorthSouth, EastWest, AscendingEast, AscendingWest, AscendingNorth, AscendingSouth,
            SouthEast, SouthWest, NorthWest, NorthEast
        }
    }
}
