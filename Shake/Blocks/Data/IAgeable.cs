﻿namespace ShakeAPI.Blocks.Data
{
    public interface IAgeable : IBlockData
    {
        int Age { get; set; }
        int MaximumAge { get; }
    }
}
