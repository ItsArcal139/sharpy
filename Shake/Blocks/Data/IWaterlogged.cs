﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks.Data
{
    public interface IWaterlogged : IBlockData
    {
        bool Waterlogged { get; set; }
    }
}
