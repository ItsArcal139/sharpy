﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks
{
    public enum Biome
    {
        Swampland,
        Forest,
        Taiga,
        Desert,
        Plains,
        Hell,
        Sky,
        Ocean,
        River,
        ExtremeHills,
        FrozenOcean,
        FrozenRiver,
        IcePlains,
        IceMountains,
        MushroomIsland,
        MushroomShore,
        Beach,
        DesertHills,
        ForestHills,
        TaigaHills,
        SmallMountains,
        Jungle,
        JungleHills,
        JungleEdge,
        DeepOcean,
        LegacyStoneBeach,
        ColdBeach,
        BirchForest,
        BirchForestHills,
        RoofedForest,
        ColdTaiga,
        ColdTaigaHills,
        MegaTaiga,
        MegaTaigaHills,
        ExtremeHillsPlus,
        Savanna,
        SavannaPlateau,
        Mesa,
        MesaPlateauForest,
        MesaPlateau,
        SunflowerPlains,
        DesertMountians,
        FlowerForest,
        TaigaMountains,
        SwamplandMountains,
        IcePlainsSpikes,
        JungleMountains,
        JungleEdgeMountains,
        ColdTaigaMountains,
        SavannaMountains,
        SavannaPlateauMountains,
        MesaBryce,
        MesaPlateauForestMountains,
        MesaPlateauMountains,
        BirchForestMountains,
        BirchForestHillsMountains,
        RoofedForestMountains,
        MegaSpruceTaiga,
        ExtremeHillsMountains,
        ExtremeHillsPlusMountains,
        MegaSpruceTaigaHills
    }
}
