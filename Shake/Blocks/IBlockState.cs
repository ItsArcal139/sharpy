﻿using ShakeAPI.Materials;
using ShakeAPI.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks
{
    public interface IBlockState : IMetadatable
    {
        IBlock Block { get; } // GetBlock();
        MaterialData Data { get; set; } // GetData();
        Material Type { get; set; } // GetType();
        [Obsolete]
        int TypeId { get; set; } // GetTypeId();
        byte LightLevel { get; } // GetLightLevel();
        IWorld World { get; } // GetWorld();
        int X { get; } // GetX();
        int Y { get; } // GetY();
        int Z { get; } // GetZ();
        Location Location { get; } // GetLocation();
        Location GetLocation(Location loc);
        IChunk Chunk { get; } // GetChunk();
        // void SetData(MaterialData data);
        // void SetType(Material type);
        // [Obsolete]
        // bool SetTypeId(int type);
        bool Update(bool force = false, bool applyPhysics = true);
        [Obsolete]
        byte RawData { get; set; } // GetRawData();
        // [Obsolete]
        // void SetRawData(byte data);
    }
}
