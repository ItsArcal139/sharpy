﻿using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Blocks
{
    public class BlockFace
    {
        public static readonly BlockFace North = new BlockFace(0, 0, -1);
        public static readonly BlockFace East = new BlockFace(1, 0, 0);
        public static readonly BlockFace South = new BlockFace(0, 0, 1);
        public static readonly BlockFace West = new BlockFace(-1, 0, 0);
        public static readonly BlockFace Up = new BlockFace(0, 1, 0);
        public static readonly BlockFace Down = new BlockFace(0, -1, 0);
        public static readonly BlockFace NorthEast = new BlockFace(North, East);
        public static readonly BlockFace NorthWest = new BlockFace(North, West);
        public static readonly BlockFace SouthEast = new BlockFace(South, East);
        public static readonly BlockFace SouthWest = new BlockFace(South, West);
        public static readonly BlockFace WestNorthWest = new BlockFace(West, NorthWest);
        public static readonly BlockFace NorthNorthWest = new BlockFace(North, NorthWest);
        public static readonly BlockFace NorthNorthEast = new BlockFace(North, NorthEast);
        public static readonly BlockFace EastNorthEast = new BlockFace(East, NorthEast);
        public static readonly BlockFace EastSouthEast = new BlockFace(East, SouthEast);
        public static readonly BlockFace SouthSouthEast = new BlockFace(South, SouthEast);
        public static readonly BlockFace SouthSouthWest = new BlockFace(South, SouthWest);
        public static readonly BlockFace WestSouthWest = new BlockFace(West, SouthWest);
        public static readonly BlockFace Self = new BlockFace(0, 0, 0);

        public int ModX { get; private set; }
        public int ModY { get; private set; }
        public int ModZ { get; private set; }

        private BlockFace(int modX, int modY, int modZ)
        {
            ModX = modX;
            ModY = modY;
            ModZ = modZ;
        }

        private BlockFace(BlockFace a, BlockFace b) : this(a.ModX + b.ModX, a.ModY + b.ModY, a.ModZ + b.ModZ) { }

        public Vector Direction
        {
            get
            {
                Vector d = new Vector(ModX, ModY, ModZ);
                if (ModX != 0 && ModY != 0 && ModZ != 0)
                {
                    d.Normalize();
                }
                return d;
            }
        }

        public BlockFace GetOppositeFace()
        {
            if (Equals(South)) return North;
            if (Equals(North)) return South;
            if (Equals(West)) return East;
            if (Equals(East)) return West;
            if (Equals(Up)) return Down;
            if (Equals(Down)) return Up;

            if (Equals(NorthEast)) return SouthWest;
            if (Equals(NorthWest)) return SouthEast;
            if (Equals(SouthEast)) return NorthWest;
            if (Equals(SouthWest)) return NorthEast;

            if (Equals(WestNorthWest)) return EastSouthEast;
            if (Equals(NorthNorthWest)) return SouthSouthEast;
            if (Equals(NorthNorthEast)) return SouthSouthWest;
            if (Equals(EastNorthEast)) return WestSouthWest;
            if (Equals(EastSouthEast)) return WestNorthWest;
            if (Equals(SouthSouthEast)) return NorthNorthWest;
            if (Equals(SouthSouthWest)) return NorthNorthEast;
            if (Equals(WestSouthWest)) return EastNorthEast;

            return Self;
        }
    }
}
