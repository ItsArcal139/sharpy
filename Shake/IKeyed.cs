﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI
{
    public interface IKeyed
    {
        NamespacedKey Key { get; }
    }
}
