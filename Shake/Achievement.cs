﻿using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI
{
    /// <summary>
    /// <see cref="Achievement"/> 代表一個可給予玩家的成就。
    /// </summary>
    [Obsolete("往後的 Minecraft 將移除成就。")]
    public class Achievement : IJavaEnumLike<Achievement>
    {
        public Achievement Parent { get; private set; }

        private Achievement(Achievement parent = null)
        {
            Parent = parent;
        }

        public bool HasParent()
        {
            return Parent != null;
        }

        public static readonly Achievement OpenInventory = new Achievement();
        public static readonly Achievement MineWood = new Achievement(MineWood);
        public static readonly Achievement BuildWorkbench = new Achievement(MineWood);
        public static readonly Achievement BuildPickaxe = new Achievement(BuildWorkbench);

        public static Achievement[] Values()
        {
            return new Achievement[]
            {
                OpenInventory, MineWood, BuildWorkbench, BuildPickaxe
            };
        }

        public int Ordinal => Array.IndexOf(Values(), this);
    }
}
