﻿using ShakeAPI.Entity;
using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Projectiles
{
    public interface IProjectileSource
    {
        T LaunchProjectile<T>(GenericType<T> projectile) where T : IProjectile;
        T LaunchProjectile<T>(GenericType<T> projectile, Vector velocity) where T : IProjectile;
    }
}
