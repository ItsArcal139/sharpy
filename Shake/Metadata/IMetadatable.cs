﻿using ShakeAPI.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Metadata
{
    public interface IMetadatable
    {
        void SetMetadata(string key, IMetadataValue value);
        List<IMetadataValue> GetMetadata(string key);
        bool HasMetadata(string key);
        void RemoveMetadata(string key, IPlugin plugin);
    }
}
