﻿using ShakeAPI.Plugins;
using ShakeAPI.Util;

namespace ShakeAPI.Metadata
{
    public interface IMetadataValue
    {
        object Value();
        int AsInt();
        float AsFloat();
        double AsDouble();
        long AsLong();
        short AsShort();
        byte AsByte();
        bool AsBoolean();
        string AsString();
        IPlugin GetOwningPlugin();
        void Invalidate();
    }

    public abstract class MetadataValueAdapter : IMetadataValue
    {
        protected readonly IPlugin owningPlugin;

        protected MetadataValueAdapter(IPlugin owningPlugin)
        {
            Validate.NotNull(owningPlugin, "owningPlugin cannot be null.");
            this.owningPlugin = owningPlugin;
        }

        public abstract object Value();

        public int AsInt()
        {
            return NumberConversions.ToInt(Value());
        }

        public float AsFloat()
        {
            return NumberConversions.ToFloat(Value());
        }

        public double AsDouble()
        {
            return NumberConversions.ToDouble(Value());
        }

        public long AsLong()
        {
            return NumberConversions.ToLong(Value());
        }

        public short AsShort()
        {
            return NumberConversions.ToShort(Value());
        }

        public byte AsByte()
        {
            return NumberConversions.ToByte(Value());
        }

        public bool AsBoolean()
        {
            object value = Value();
            if(value is bool)
            {
                return (bool)value;
            }

            if(value is decimal)
            {
                return (decimal)value != 0;
            }

            if(value is string)
            {
                return bool.Parse(value.ToString());
            }
            return value != null;
        }

        public string AsString()
        {
            object value = Value();

            if(value == null)
            {
                return "";
            }
            return value.ToString();
        }

        public abstract IPlugin GetOwningPlugin();
        public abstract void Invalidate();
    }
}
