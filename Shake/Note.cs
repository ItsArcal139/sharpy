﻿using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI
{
    public class Note
    {
#pragma warning disable 0612
        public class Tone : IJavaEnumLike<Tone>
        {
            [Obsolete]
            public byte Id { get; private set; }
            public bool Sharpable { get; private set; }

            private static readonly Dictionary<byte, Tone> BY_DATA = new Dictionary<byte, Tone>();
            public const byte TONES_COUNT = 12;

            private Tone(int id, bool sharpable)
            {
                this.Id = (byte)(id % TONES_COUNT);
                this.Sharpable = sharpable;
            }

            [Obsolete]
            public byte GetId(bool sharped)
            {
                byte id = (byte)(sharped && Sharpable ? this.Id + 1 : this.Id);
                return (byte)(id % TONES_COUNT);
            }

            [Obsolete]
            public bool IsSharped(byte id)
            {
                if(id == GetId(false))
                {
                    return false;
                } else if(id == GetId(true))
                {
                    return true;
                } else
                {
                    throw new ArgumentException("The id isn't matching to the tone.");
                }
            }

            [Obsolete]
            public static Tone GetById(byte id)
            {
                return BY_DATA[id];
            }

            public static readonly Tone G = new Tone(0x1, true);
            public static readonly Tone A = new Tone(0x3, true);
            public static readonly Tone B = new Tone(0x5, false);
            public static readonly Tone C = new Tone(0x6, true);
            public static readonly Tone D = new Tone(0x8, true);
            public static readonly Tone E = new Tone(0xa, false);
            public static readonly Tone F = new Tone(0xb, true);

            public static Tone[] Values()
            {
                return new Tone[]
                {
                    G, A, B, C, D, E, F
                };
            }

            public int Ordinal => Array.IndexOf(Values(), this);

            public override string ToString()
            {
                switch(Id)
                {
                    case 0x1: return "G";
                    case 0x3: return "A";
                    case 0x5: return "B";
                    case 0x6: return "C";
                    case 0x8: return "D";
                    case 0xa: return "E";
                    case 0xb: return "F";
                }
                return "?";
            }

            static Tone()
            {
                foreach(Tone tone in Values())
                {
                    int id = tone.Id % TONES_COUNT;
                    BY_DATA.Add((byte)id, tone);

                    if(tone.Sharpable)
                    {
                        id = (id + 1) % TONES_COUNT;
                        BY_DATA.Add((byte)id, tone);
                    }
                }
            }
        }

        public byte Id { get; private set; }

        public Note(int note)
        {
            Validate.IsTrue(note >= 0 && note <= 24, "The note value has to be between 0 and 24.");
            Id = (byte)note;
        }

        public Note(int octave, Tone tone, bool sharped)
        {
            if(sharped && !tone.Sharpable)
            {
                tone = Tone.Values()[tone.Ordinal + 1];
                sharped = false;
            }

            if(octave < 0 || octave > 2 || (octave == 2 && !(tone.Equals(Tone.F) && sharped)))
            {
                throw new ArgumentException("Tone and octave have to be between F#0 and F#2");
            }

            Id = (byte)(octave * Tone.TONES_COUNT + tone.GetId(sharped));
        }

        public static Note Flat(int octave, Tone tone)
        {
            Validate.IsTrue(octave != 2, "Octave cannot be 2 for flats.");
            tone = tone.Equals(Tone.G) ? Tone.F : Tone.Values()[tone.Ordinal - 1];
            return new Note(octave, tone, tone.Sharpable);
        }

        public static Note Sharp(int octave, Tone tone)
        {
            return new Note(octave, tone, true);
        }

        public static Note Natural(int octave, Tone tone)
        {
            Validate.IsTrue(octave != 2, "Octave cannot be 2 for naturals.");
            return new Note(octave, tone, false);
        }

        public Note Sharped()
        {
            Validate.IsTrue(Id < 24, "This note cannot be sharped because it is the highest known note!");
            return new Note(Id + 1);
        }

        public Note Flattened()
        {
            Validate.IsTrue(Id > 0, "This note cannot be flattened because it is the lowest known note!");
            return new Note(Id - 1);
        }

        public int Octave => Id / Tone.TONES_COUNT;

        private byte ToneByte => (byte)(Id % Tone.TONES_COUNT);

        public Tone GetTone()
        {
            return Tone.GetById(ToneByte);
        }

        public bool IsSharped()
        {
            return GetTone().IsSharped(ToneByte);
        }

        public override int GetHashCode()
        {
            /*
            int prime = 31;
            int result = 1;

            result = prime * result + Id;
            return result; */
            return 31 + Id;
        }

        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (GetType() != obj.GetType()) return false;

            Note other = (Note)obj;
            if (Id != other.Id) return false;
            return true;
        }

        public override string ToString()
        {
            return string.Format("Note{{{0}}}", GetTone().ToString() + (IsSharped() ? "#" : ""));
        }
#pragma warning restore 0612
    }
}
