﻿using ShakeAPI.Plugins;
using ShakeAPI.Util;
using System;
using System.Text.RegularExpressions;

namespace ShakeAPI
{
    public sealed class NamespacedKey
    {
        public const string Minecraft = "minecraft";
        public const string Shake = "shake";

        private static readonly Regex validNamespace = new Regex("[a-z0-9._-]+");
        private static readonly Regex validKey = new Regex("[a-z0-9/._-]+");

        public string Namespace { get; private set; }
        public string Key { get; private set; }

        [Obsolete]
        public NamespacedKey(string @namespace, string key)
        {
            Validate.NotNull(@namespace, "Namespace cannot be null.");
            Validate.NotNull(key, "Key cannot be null.");

            Validate.IsTrue(!validNamespace.IsMatch(@namespace), $"Invalid namespace. Must be [a-z0-9._-]: {@namespace}");
            Validate.IsTrue(!validKey.IsMatch(key), $"Invalid key. Must be [a-z0-9/._-]: {key}");

            Namespace = @namespace;
            Key = key;

            string str = ToString();
            Validate.Assert<ArgumentException>(str.Length < 256, "NamespacedKey must be less than 256 characters.");
        }

        public NamespacedKey(IPlugin plugin, string key)
        {
            Validate.NotNull(plugin, "Plugin cannot be null.");
            Validate.NotNull(key, "Key cannot be null.");

            Namespace = plugin.Name.ToLower();
            Key = key.ToLower();

            Validate.IsTrue(!validNamespace.IsMatch(Namespace), $"Invalid namespace. Must be [a-z0-9._-]: {Namespace}");
            Validate.IsTrue(!validKey.IsMatch(Key), $"Invalid key. Must be [a-z0-9/._-]: {Key}");

            string str = ToString();
            Validate.Assert<ArgumentException>(str.Length < 256, "NamespacedKey must be less than 256 characters.");
        }

        public override int GetHashCode()
        {
            int hash = 5;
            hash = 47 * hash + Namespace.GetHashCode();
            hash = 47 * hash + Key.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            return Namespace + ":" + Key;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj.GetType().Equals(GetType()))
            {
                NamespacedKey other = (NamespacedKey)obj;
                return other.Namespace.Equals(Namespace) && other.Key.Equals(Key);
            }
            return false;
        }

        [Obsolete]
        public static NamespacedKey RandomKey()
        {
            return new NamespacedKey(Shake, Uuid.RandomUuid().ToString());
        }

#pragma warning disable 0612
        public static NamespacedKey MinecraftKey(string key)
        {
            return new NamespacedKey(Minecraft, key);
        }
#pragma warning restore 0612
    }
}
