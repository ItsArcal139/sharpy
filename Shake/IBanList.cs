﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI
{
    public interface IBanList
    {
        IBanEntry GetBanEntry(string target);
        IBanEntry AddBan(string target, string reason, DateTime expires, string source);
        ICollection<IBanEntry> GetBanEntries();
        bool IsBanned(string target);
        void Pardon(string target);
    }

    public static class BanListHelper
    {
        public enum Type
        {
            Name, IP
        }
    }
}
