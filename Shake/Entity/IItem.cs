﻿using ShakeAPI.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Entity
{
    public interface IItem : IEntity
    {
        ItemStack ItemStack { get; set; }
        int PickupDelay { get; set; }
    }
}
