﻿using ShakeAPI.Blocks;
using ShakeAPI.Commands;
using ShakeAPI.Conversations;
using ShakeAPI.Events.Entity;
using ShakeAPI.Inventory;
using ShakeAPI.Permissions;
using ShakeAPI.Projectiles;
using ShakeAPI.Util;
using System;
using System.Collections.Generic;

using TeleportCause = ShakeAPI.Events.Player.PlayerTeleportEvent.TeleportCause;

namespace ShakeAPI.Entity
{
    public interface IEntity
    {
        Location Location { get; }
        Location GetLocation(Location loc);
        Vector Velocity { get; set; }
        bool IsOnGround { get; }
        IWorld World { get; }
        bool Teleport(Location loc);
        bool Teleport(Location loc, TeleportCause cause);
        bool Teleport(IEntity entity, TeleportCause cause);
        List<IEntity> GetNearbyEntities(double x, double y, double z);
        int Id { get; }
        int FireTicks { get; set; }
        int MaxFireTicks { get; }
        void Remove();
        bool IsDead { get; }
        bool IsValid { get; }
        IServer Server { get; }
        List<IEntity> Passengers { get; }

        /// <summary>
        /// Check if a vehicle has passengers.
        /// </summary>
        /// <returns>True if the vehicle has no passengers.</returns>
        bool IsEmpty { get; }
        bool Eject();
        float FallDistance { get; set; }
        EntityDamageEvent LastDamageCause { get; set; }
        Uuid UniqueId { get; }
        int TicksLived { get; set; }
        void PlayEffect(EntityEffect effect);
        EntityType Type { get; }
        bool IsInsideVehicle { get; }
        bool LeaveVehicle();
        IEntity Vehicle { get; }
    }

    public interface IDamageable : IEntity
    {
        void Damage(double amount);
        void Damage(double amount, IEntity source);
        double Health { get; set; }
        double MaxHealth { get; set; }
        void ResetMaxHealth();
    }

    public interface ILivingEntity : IEntity, IDamageable, IProjectileSource
    {
        double GetEyeHeight();
        double GetEyeHeight(bool ignoreSneaking);
        Location GetEyeLocation();
        [Obsolete]
        ICollection<IBlock> GetLineOfSight(ICollection<byte> transparent, int maxDistance);
        [Obsolete]
        IBlock GetTargetBlock(ICollection<byte> transparent, int maxDistance);
        [Obsolete]
        ICollection<IBlock> GetLastTwoTargetBlocks(ICollection<byte> transparent, int maxDistance);
        // throwEgg(): IEgg
        // throwSnowball(): ISnowball
        // shootArrow(): IArrow

        /// <summary>
        /// The amount of air that the living entity has remaining, in ticks.
        /// </summary>
        int RemainingAir { get; set; }

        int MaximumAir { get; set; }
        int MaximumNoDamageTicks { get; set; }
        double LastDamage { get; set; }
        int NoDamageTicks { get; set; }
        IPlayer Killer { get; }
        bool AddPotionEffect(PotionEffect effect);
        bool AddPotionEffect(PotionEffect effect, bool force);
        bool AddPotionEffects(ICollection<PotionEffect> effects);
        bool HasPotionEffect(PotionEffectType type);
        bool RemovePotionEffect(PotionEffectType type);
        ICollection<PotionEffect> GetActivePotionEffects();
        bool HasLineOfSight(IEntity other);
        bool RemovesWhenFarAway { get; set; }
        EntityEquipment GetEquipment();
        bool CanPickupItems { get; set; }
        string CustomName { get; set; }
        bool CustomNameVisible { get; set; }
        bool IsLeashed { get; }

        /// <summary>
        /// The entity that is currently leading this entity.
        /// </summary>
        /// <exception cref="InvalidOperationException">Throws if not currently leashed.</exception>
        IEntity LeashHolder { get; set; }
    }

    public class EntityEquipment
    {

    }

    public interface IHumanEntity : ILivingEntity, IInventoryHolder, IAnimalTamer, IPermissible
    {
        string Name { get; set; }
    }

    public interface IPlayer : IEntity, IHumanEntity, IConversable, ICommandSender, IOfflinePlayer
    {

    }
}
