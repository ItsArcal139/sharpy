﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShakeAPI.Inventory;

namespace ShakeAPI.Enchantments
{
    public class EnchantmentWrapper : Enchantment
    {
        public EnchantmentWrapper(string name) : base(NamespacedKey.MinecraftKey(name))
        {
            ;
        }

        public Enchantment Enchantment => GetByKey(Key);

        public override int MaxLevel => Enchantment.MaxLevel;

        public override int StartLevel => Enchantment.StartLevel;

        public override EnchantmentTarget ItemTarget => Enchantment.ItemTarget;

        public override bool CanEnchantItem(ItemStack stack)
        {
            return Enchantment.CanEnchantItem(stack);
        }

        [Obsolete]
        public override string Name => Enchantment.Name;

        public override bool IsTreasure()
        {
            return Enchantment.IsTreasure();
        }

        [Obsolete]
        public override bool IsCursed()
        {
            return Enchantment.IsCursed();
        }

        public override bool ConflictsWith(Enchantment older)
        {
            return Enchantment.ConflictsWith(older);
        }
    }
}
