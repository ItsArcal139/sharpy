﻿using ShakeAPI.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Enchantments
{
    public enum EnchantmentTarget
    {
        All,
        Armor,
        ArmorFeet,
        ArmorLegs,
        ArmorTorso, // ?
        ArmorHead,
        Weapon,
        Tool,
        Bow,
        FishingRod
    }

    public delegate bool EnchantmentTargetDelegate(Material item);

#pragma warning disable 0612
    public static class EnchantmentTargetHelper
    {
        private static Dictionary<string, EnchantmentTargetDelegate> map = new Dictionary<string, EnchantmentTargetDelegate>();

        static EnchantmentTargetHelper()
        {
            map.Add("All", (item) => true);
            map.Add("ArmorFeet", (item) =>
            {
                return item.Equals(Material.LegacyLeatherBoots)
                || item.Equals(Material.LegacyChainmailBoots)
                || item.Equals(Material.LegacyIronBoots)
                || item.Equals(Material.LegacyDiamondBoots)
                || item.Equals(Material.LegacyGoldBoots);
            });
            map.Add("ArmorLegs", (item) =>
            {
                return item.Equals(Material.LegacyLeatherLeggings)
                    || item.Equals(Material.LegacyChainmailLeggings)
                    || item.Equals(Material.LegacyIronLeggings)
                    || item.Equals(Material.LegacyDiamondLeggings)
                    || item.Equals(Material.LegacyGoldLeggings);
            });
            map.Add("ArmorTorso", (item) =>
            {
                return item.Equals(Material.LegacyLeatherChestplate)
                    || item.Equals(Material.LegacyChainmailChestplate)
                    || item.Equals(Material.LegacyIronChestplate)
                    || item.Equals(Material.LegacyDiamondChestplate)
                    || item.Equals(Material.LegacyGoldChestplate);
            });
            map.Add("ArmorHead", (item) =>
            {
                return item.Equals(Material.LegacyLeatherHelmet)
                    || item.Equals(Material.LegacyChainmailHelmet)
                    || item.Equals(Material.LegacyIronHelmet)
                    || item.Equals(Material.LegacyDiamondHelmet)
                    || item.Equals(Material.LegacyGoldHelmet);
            });
            map.Add("Armor", (item) =>
            {
                return map["ArmorFeet"](item)
                    || map["ArmorLegs"](item)
                    || map["ArmorTorso"](item)
                    || map["ArmorHead"](item);
            });
            map.Add("Weapon", (item) =>
            {
                return item.Equals(Material.LegacyWoodenSword)
                    || item.Equals(Material.LegacyStoneSword)
                    || item.Equals(Material.LegacyIronSword)
                    || item.Equals(Material.LegacyDiamondSword)
                    || item.Equals(Material.LegacyGoldSword);
            });
            map.Add("Tool", (item) =>
            {
                Material[] tools = new Material[] {
                    Material.LegacyWoodenShovel,
                    Material.LegacyStoneShovel,
                    Material.LegacyIronShovel,
                    Material.LegacyDiamondShovel,
                    Material.LegacyGoldShovel,

                    // @start - Non-vanilla
                    Material.LegacyWoodenHoe,
                    Material.LegacyStoneHoe,
                    Material.LegacyIronHoe,
                    Material.LegacyDiamondHoe,
                    Material.LegacyGoldHoe,
                    // @end - Non-vanilla

                    Material.LegacyWoodenAxe,
                    Material.LegacyStoneAxe,
                    Material.LegacyIronAxe,
                    Material.LegacyDiamondAxe,
                    Material.LegacyGoldAxe,

                    Material.LegacyWoodenPickaxe,
                    Material.LegacyStonePickaxe,
                    Material.LegacyIronPickaxe,
                    Material.LegacyDiamondPickaxe,
                    Material.LegacyGoldPickaxe,

                    // @start - Non-vanilla
                    Material.LegacyShears,
                    Material.LegacyFlintAndSteel
                    // @end - Non-vanilla
                };

                bool result = false;
                foreach (Material i in tools)
                {
                    result = result || item.Equals(i);
                }
                return result;
            });
            map.Add("Bow", (item) => item.Equals(Material.LegacyBow));
            map.Add("FishingRod", (item) => item.Equals(Material.LegacyFishingRod));
        }
#pragma warning restore 0612

        public static bool Includes(this EnchantmentTarget target, ItemStack stack)
        {
            return map[Enum.GetName(typeof(EnchantmentTarget), target)](stack.Type);
        }
    }
}
