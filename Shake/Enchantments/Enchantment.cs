﻿using ShakeAPI.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Enchantments
{
    public abstract class Enchantment : IKeyed
    {
        /// <summary>
        /// 給予環境保護。
        /// </summary>
        public static readonly Enchantment ProtectionEnvironmental = new EnchantmentWrapper("protection");

        /// <summary>
        /// 給予火焰保護。
        /// </summary>
        public static readonly Enchantment ProtectionFire = new EnchantmentWrapper("fire_protection");

        /// <summary>
        /// 給予落地保護。
        /// </summary>
        public static readonly Enchantment ProtectionFall = new EnchantmentWrapper("feather_falling");

        /// <summary>
        /// 給予爆炸保護。
        /// </summary>
        public static readonly Enchantment ProtectionExplosions = new EnchantmentWrapper("blast_protection");

        /// <summary>
        /// 給予投射物保護。
        /// </summary>
        public static readonly Enchantment ProtectionProjectile = new EnchantmentWrapper("projectile_protection");

        public static readonly Enchantment Oxygen = new EnchantmentWrapper("respiration");

        public static readonly Enchantment WaterWorker = new EnchantmentWrapper("aqua_affinity");

        public static readonly Enchantment Thorns = new EnchantmentWrapper("thorns");

        public static readonly Enchantment DepthStrider = new EnchantmentWrapper("depth_strider");

        public static readonly Enchantment FrostWalker = new EnchantmentWrapper("frost_walker");

        public static readonly Enchantment BindingCurse = new EnchantmentWrapper("binding_curse");

        public static readonly Enchantment DamageAll = new EnchantmentWrapper("sharpness");

        public static readonly Enchantment DamageUndead = new EnchantmentWrapper("smite");

        public static readonly Enchantment DamageArthropods = new EnchantmentWrapper("bane_of_arthropods");

        public static readonly Enchantment Knockback = new EnchantmentWrapper("knockback");

        public static readonly Enchantment FireAspect = new EnchantmentWrapper("fire_aspect");

        public static readonly Enchantment LootBonusMobs = new EnchantmentWrapper("looting");

        public static readonly Enchantment SweepingEdge = new EnchantmentWrapper("sweeping_edge");

        public static readonly Enchantment DigSpeed = new EnchantmentWrapper("efficiency");

        /// <summary>
        /// 允許方塊以目前型態掉落物品，而非「分解物」。
        /// </summary>
        /// <example>掉落石頭而非鵝卵石。</example>
        public static readonly Enchantment SilkTouch = new EnchantmentWrapper("silk_touch");

        public static readonly Enchantment Durability = new EnchantmentWrapper("unbreaking");

        public static readonly Enchantment LootBonusBlocks = new EnchantmentWrapper("fortune");

        public static readonly Enchantment ArrowDamage = new EnchantmentWrapper("power");

        public static readonly Enchantment ArrowKnockback = new EnchantmentWrapper("punch");

        public static readonly Enchantment ArrowFire = new EnchantmentWrapper("flame");

        public static readonly Enchantment ArrowInfinite = new EnchantmentWrapper("infinity");

        public static readonly Enchantment Luck = new EnchantmentWrapper("luck_of_the_sea");

        public static readonly Enchantment Lure = new EnchantmentWrapper("lure");

        public static readonly Enchantment Loyalty = new EnchantmentWrapper("loyalty");

        public static readonly Enchantment Impaling = new EnchantmentWrapper("impaling");

        public static readonly Enchantment Riptide = new EnchantmentWrapper("riptide");

        public static readonly Enchantment Channeling = new EnchantmentWrapper("channeling");

        public static readonly Enchantment Mending = new EnchantmentWrapper("mending");

        public static readonly Enchantment VanishingCurse = new EnchantmentWrapper("vanishing_curse");

        // private static readonly Dictionary<int, Enchantment> byId = new Dictionary<int, Enchantment>();
        private static readonly Dictionary<NamespacedKey, Enchantment> byKey = new Dictionary<NamespacedKey, Enchantment>();
        private static readonly Dictionary<string, Enchantment> byName = new Dictionary<string, Enchantment>();
        private static bool acceptingNew = true;

        public NamespacedKey Key { get; private set; }

        [Obsolete("Enchantments are badly named, use Key property.")]
        public abstract string Name { get; }

        public Enchantment(NamespacedKey key)
        {
            Key = key;
        }

        /// <summary>
        /// 這個附魔最高可達到的等級。
        /// </summary>
        public abstract int MaxLevel { get; }

        /// <summary>
        /// 這個附魔的初始等級。
        /// </summary>
        public abstract int StartLevel { get; }

        /// <summary>
        /// 可套用這個附魔的 <see cref="ItemStack"/> 種類。
        /// </summary>
        public abstract EnchantmentTarget ItemTarget { get; }

        public abstract bool IsTreasure();

        [Obsolete]
        public abstract bool IsCursed();

        /// <summary>
        /// 確認這個附魔是否與現有的某一附魔互相衝突。
        /// </summary>
        /// <param name="older">欲比較之現有附魔。</param>
        /// <returns>若會衝突則回傳 true。</returns>
        public abstract bool ConflictsWith(Enchantment older);

        public abstract bool CanEnchantItem(ItemStack stack);

#pragma warning disable 0612
        public override bool Equals(object obj)
        {
            if(obj != null && obj is Enchantment)
            {
                Enchantment other = (Enchantment)obj;
                return Key.Equals(other.Key);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

#pragma warning disable 0618
        public override string ToString()
        {
            return $"Enchantment[{Key}, {Name}]";
        }

        public static void RegisterEnchantment(Enchantment enchantment)
        {
            if(byKey.ContainsKey(enchantment.Key) || byName.ContainsKey(enchantment.Name))
            {
                throw new ArgumentException("Cannot set already-set enchantment.");
            } else if(!IsAcceptingRegistrations())
            {
                throw new InvalidOperationException("No longer accepting new enchantments.");
            }

            byKey.Add(enchantment.Key, enchantment);
            byName.Add(enchantment.Name, enchantment);
        }
#pragma warning restore 0618

        public static bool IsAcceptingRegistrations()
        {
            return acceptingNew;
        }

        public static void StopAcceptingRegistrations()
        {
            acceptingNew = false;
            // EnchantCommand.buildEnchantments();
        }
        
#pragma warning restore 0612

        public static Enchantment GetByKey(NamespacedKey key)
        {
            return byKey[key];
        }

        public static Enchantment GetByName(string name)
        {
            return byName[name];
        }

        public static Enchantment[] Values()
        {
            return byName.Values.ToArray();
        }

    }
}
