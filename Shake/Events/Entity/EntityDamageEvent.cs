﻿using ShakeAPI.Entity;
using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShakeAPI.Events.Entity
{
    public class EntityDamageEvent : EntityEvent, ICancellable
    {
        public bool Cancelled { get; set; }

        private static readonly HandlerList handlers = new HandlerList();

        private readonly Dictionary<DamageModifier, double> modifiers = new Dictionary<DamageModifier, double>();

        private readonly Dictionary<DamageModifier, Func<double, double>> modifierFunctions = new Dictionary<DamageModifier, Func<double, double>>();

        private readonly Dictionary<DamageModifier, double> originals = new Dictionary<DamageModifier, double>();

        // TODO: more members.

        private static Dictionary<DamageModifier, double> GetDefaultModifiers(double damage)
        {
            Dictionary<DamageModifier, double> m = new Dictionary<DamageModifier, double>();
            m.Add(DamageModifier.Base, damage);
            return m;
        }

        private static Dictionary<DamageModifier, Func<double, double>> GetDefaultModifierFunctions(double damage)
        {
            Dictionary<DamageModifier, Func<double, double>> m = new Dictionary<DamageModifier, Func<double, double>>();
            m.Add(DamageModifier.Base, d => 0);
            return m;
        }

        public EntityDamageEvent(IEntity damagee, DamageCause cause, double damage) : this(damagee, cause, GetDefaultModifiers(damage), GetDefaultModifierFunctions(damage)) { }

        public EntityDamageEvent(IEntity damagee, DamageCause cause, Dictionary<DamageModifier, double> modifiers, Dictionary<DamageModifier, Func<double, double>> modifierFunctions) : base(damagee)
        {
            Validate.IsTrue(!modifiers.Keys.Contains(DamageModifier.Base), "DamageModifier.Base missing in modifiers.");
            Validate.IsTrue(modifiers.Keys.Equals(modifierFunctions.Keys), "Must have a modifier function for each DamageModifier.");
            foreach (Func<double, double> f in modifierFunctions.Values)
            {
                Validate.NotNull(f, "Cannot have null modifier function.");
            }

            originals = new Dictionary<DamageModifier, double>(modifiers);
            Cause = cause;
            this.modifiers = modifiers;
            this.modifierFunctions = modifierFunctions;
        }

        public DamageCause Cause { get; set; }

        public double GetOriginalDamage(DamageModifier type)
        {
            double damage;
            if (originals.TryGetValue(type, out damage))
            {
                return damage;
            }
            return 0;
        }

        public double Damage
        {
            get => GetDamage();
            set => SetDamage(value);
        }

        private double GetDamage()
        {
            return GetDamage(DamageModifier.Base);
        }

        public double GetDamage(DamageModifier type)
        {
            Validate.NotNull(type, "Cannot have null DamageModifier.");
            double damage;
            if (modifiers.TryGetValue(type, out damage))
            {
                return damage;
            }
            return 0;
        }

        public void SetDamage(DamageModifier type, double damage)
        {
            if (!modifiers.Keys.Contains(type))
            {
                throw new NotSupportedException($"{type.ToString()} is not applicable to {Entity.ToString()}.");
            }
            modifiers[type] = damage;
        }

        private void SetDamage(double damage)
        {
            double remaining = damage;
            double oldRemaining = Damage;

            foreach (DamageModifier m in Enum.GetValues(typeof(DamageModifier)))
            {
                if (!IsApplicable(m))
                {
                    continue;
                }

                Func<double, double> func = modifierFunctions[m];
                double newVanilla = func(remaining);
                double oldVanilla = func(oldRemaining);
                double diff = oldVanilla - newVanilla;

                double old = GetDamage(m);
                if (old > 0)
                {
                    SetDamage(m, Math.Max(0, old - diff));
                }
                else
                {
                    SetDamage(m, Math.Min(0, old - diff));
                }

                remaining += newVanilla;
                oldRemaining += oldVanilla;
            }

            SetDamage(DamageModifier.Base, damage);
        }

        public bool IsApplicable(DamageModifier type)
        {
            return modifiers.Keys.Contains(type);
        }

        public override HandlerList Handlers => handlers;

        public static HandlerList GetHandlerList()
        {
            return handlers;
        }

        public enum DamageModifier
        {
            Base, HardHat, Blocking, Armor, Resistance, Magic, Absorption
        }

        public enum DamageCause
        {
            Contact, EntityAttack, EntitySweepAttack, Projectile, Suffocation, Fall,
            Fire, FireTick, Melting, Lava, Drowing, BlockExplotion, EntityExplotion,
            Void, Lightning, Suicide, Starvation, Poison, Magic, Wither, FallingBlock,
            Thorns, DragonBreath, Custom, FlyIntoWall, HotFloor, Cramming, Dryout
        }
    }
}
