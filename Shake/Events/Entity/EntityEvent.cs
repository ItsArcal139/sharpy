﻿using ShakeAPI.Entity;

namespace ShakeAPI.Events.Entity
{
    public abstract class EntityEvent : Event
    {
        protected IEntity entity;

        public EntityEvent(IEntity what)
        {
            entity = what;
        }

        public IEntity Entity => entity;

        public EntityType EntityType => entity.Type;
    }
}
