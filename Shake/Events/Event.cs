﻿namespace ShakeAPI.Events
{
    public abstract class Event
    {
        public string Name => GetType().Name;
        private readonly bool async;

        public Event(bool async = false)
        {
            this.async = async;
        }

        public abstract HandlerList Handlers { get; }

        public bool IsAsync() => async;

        public enum Result
        {
            Deny = -1,
            Default,
            Allow
        }
    }

    public enum EventPriority
    {
        Lowest, Low, Normal, High, Highest, Monitor
    }
}
