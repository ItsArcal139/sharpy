﻿using ShakeAPI.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShakeAPI.Events
{
    public class HandlerList
    {
        private volatile RegisteredListener[] handlers = null;
        private Dictionary<EventPriority, List<RegisteredListener>> handlerSlots;
        private static List<HandlerList> allLists = new List<HandlerList>();

        public static void BakeAll()
        {
            lock (allLists)
            {
                foreach (HandlerList h in allLists)
                {
                    h.Bake();
                }
            }
        }

        public static void UnregisterAll()
        {
            lock (allLists)
            {
                foreach (HandlerList h in allLists)
                {
                    lock (h)
                    {
                        foreach (List<RegisteredListener> list in h.handlerSlots.Values)
                        {
                            list.Clear();
                        }
                        h.handlers = null;
                    }
                }
            }
        }

        public static void UnregisterAll(IPlugin plugin)
        {
            lock (allLists)
            {
                foreach (HandlerList h in allLists)
                {
                    h.Unregister(plugin);
                }
            }
        }

        public static void UnregisterAll(IListener listener)
        {
            lock (allLists)
            {
                foreach (HandlerList h in allLists)
                {
                    h.Unregister(listener);
                }
            }
        }

        public HandlerList()
        {
            handlerSlots = new Dictionary<EventPriority, List<RegisteredListener>>();
            foreach (EventPriority o in Enum.GetValues(typeof(EventPriority)))
            {
                handlerSlots.Add(o, new List<RegisteredListener>());
            }

            lock (allLists)
            {
                allLists.Add(this);
            }
        }

        private object lockObj = new object();

        public void Register(RegisteredListener listener)
        {
            lock (lockObj)
            {
                if (handlerSlots[listener.Priority].Contains(listener))
                {
                    throw new InvalidOperationException($"This listener is already registered to priority {listener.Priority.ToString()}");
                }
                handlers = null;
                handlerSlots[listener.Priority].Add(listener);
            }
        }

        public void RegisterAll(ICollection<RegisteredListener> listeners)
        {
            foreach (RegisteredListener listener in listeners)
            {
                this.Register(listener);
            }
        }

        public void Unregister(RegisteredListener listener)
        {
            lock (lockObj)
            {
                if (handlerSlots[listener.Priority].Remove(listener))
                {
                    handlers = null;
                }
            }
        }

        public void Unregister(IPlugin plugin)
        {
            lock (lockObj)
            {
                bool changed = false;
                foreach(List<RegisteredListener> list in handlerSlots.Values)
                {
                    foreach(RegisteredListener listener in list)
                    {
                        if (listener.Plugin.Equals(plugin)) changed = true;
                    }
                }
                if (changed) handlers = null;
            }
        }

        public void Unregister(IListener listener)
        {
            lock (lockObj)
            {
                bool changed = false;
                foreach (List<RegisteredListener> list in handlerSlots.Values)
                {
                    foreach (RegisteredListener l in list)
                    {
                        if (l.Listener.Equals(listener)) changed = true;
                    }
                }
                if (changed) handlers = null;
            }
        }

        public void Bake()
        {
            lock(lockObj)
            {
                if (handlers != null) return;
                List<RegisteredListener> entries = new List<RegisteredListener>();
                foreach(KeyValuePair<EventPriority, List<RegisteredListener>> entry in handlerSlots.ToList())
                {
                    entries.AddRange(entry.Value);
                }
                handlers = entries.ToArray();
            }
        }

        // TODO: https://github.com/Bukkit/Bukkit/blob/master/src/main/java/org/bukkit/event/HandlerList.java#L184

        public static List<HandlerList> GetHandlerLists()
        {
            lock(allLists)
            {
                return allLists.ToList();
            }
        }
    }
}
