﻿namespace ShakeAPI.Events
{
    public interface ICancellable
    {
        bool Cancelled { get; set; }
    }
}
