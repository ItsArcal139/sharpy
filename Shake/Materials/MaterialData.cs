﻿using ShakeAPI.Inventory;
using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Materials
{
#pragma warning disable 0612
    public class MaterialData : ICloneableG<MaterialData>
    {
        public Material Type { get; private set; }

        [Obsolete]
        public byte Data { get; private set; }

        public MaterialData(Material type) : this(type, 0) { }

        [Obsolete]
        public MaterialData(Material type, byte data)
        {

            Type = type;
            Data = data;
        }

        public Material ItemType => Type;

        public ItemStack ToItemStack(int amount = 0)
        {
            return new ItemStack(ItemType, amount, Data);
        }

        public override string ToString()
        {
            return ItemType.ToString() + "(" + Data + ")";
        }

        public override int GetHashCode()
        {
            return (ItemType.GetHashCode() << 8) ^ Data;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is MaterialData)
            {
                MaterialData md = (MaterialData)obj;
                return md.ItemType == ItemType && md.Data == Data;
            }
            return false;
        }

        public MaterialData Clone()
        {
            return (MaterialData)MemberwiseClone();
        }
    }
#pragma warning restore 0612
}
