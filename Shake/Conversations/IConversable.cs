﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Conversations
{
    public interface IConversable
    {
        bool IsConversing { get; }

        void AcceptConversationInput(string input);

        bool BeginConversation(Conversation conversation);

        void AbandonConversation(Conversation conversation, ConversationAbandonEvent details);

        void SendRawMessage(string message);
    }

    public class ConversationAbandonEvent
    {

        // TODO: https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/browse/src/main/java/org/bukkit/conversations/ConversationAbandonedEvent.java
    }

    public class Conversation
    {
        // TODO: https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/browse/src/main/java/org/bukkit/conversations/Conversation.java
    }
}
