﻿using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Plugins
{
    public interface IPlugin
    {
        string DataFolder { get; }
        void SaveConfig();
        void ReloadConfig();
        PluginLoader PluginLoader { get; }
        IServer Server { get; }
        bool Enabled { get; }

        void OnDisable();
        void OnLoad();
        void OnEnable();

        bool Naggable { get; set; }
        Logger Logger { get; }
        string Name { get; }
    }

    public class PluginLoader
    {

    }
}
