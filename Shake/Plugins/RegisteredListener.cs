﻿using ShakeAPI.Events;
using ShakeAPI.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Plugins
{
    public class RegisteredListener
    {
        public IListener Listener { get; private set; }
        public EventPriority Priority { get; private set; }
        public IPlugin Plugin { get; private set; }

        private IEventExecutor executor;
        private bool ignoreCancelled;   

        public RegisteredListener(IListener listener, IEventExecutor executor, EventPriority priority, IPlugin plugin, bool ignoreCancelled)
        {
            Listener = listener;
            this.executor = executor;
            Priority = priority;
            Plugin = plugin;
            this.ignoreCancelled = ignoreCancelled;
        }
    }

    public interface IEventExecutor
    {
        void Execute(IListener listener, Event @event);
    }

    public interface IPluginManager
    {
        // void RegisterInterface()
        IPlugin GetPlugin(string name);
        IPlugin[] GetPlugins();
        bool IsPluginEnabled(string name);
        bool IsPluginEnabled(IPlugin plugin);
        IPlugin LoadPlugin(string file);
        IPlugin[] LoadPlugins(string directory);
        void DisablePlugins();
        void ClearPlugins();
        void CallEvent(Event @event);
        void RegisterEvents(IListener listener, IPlugin plugin);

        // These 2 methods needs type check.
        void RegisterEvent(Type eventType, IListener listener, EventPriority priority, IEventExecutor executor, IPlugin plugin);
        void RegisterEvent(Type eventType, IListener listener, EventPriority priority, IEventExecutor executor, IPlugin plugin, bool ignoreCancelled);

        void EnablePlugin(IPlugin plugin);
        void DisablePlugin(IPlugin plugin);
        Permission GetPermission(string name);
        void AddPermission(Permission perm);
        void RemovePermission(Permission perm);
        List<Permission> GetDefaultPermissions(bool op);
        void RecalculatePermissionDefaults(Permission perm);
        void SubscribeToPermission(string permission, IPermissible permissible);
        List<IPermissible> GetPermissionSubscriptions(string permission);
        void SubscribeToDefaultPerms(bool op, IPermissible permissible);
        void UnsubscribeFromDefaultPerms(bool op, IPermissible permissible);
        List<IPermissible> GetDefaultPermSubscriptions(bool op);
        List<Permission> GetPermissions();
        bool UseTimings();
    }
}
