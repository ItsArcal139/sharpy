﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Plugins.Messaging
{
    public interface IPluginMessageRecipient
    {
        void SendPluginMessage(IPlugin source, string channel, byte[] message);
        List<string> GetListeningPluginChannels();
    }
}
