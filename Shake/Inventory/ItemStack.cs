﻿using ShakeAPI.Entity;
using ShakeAPI.Events.Inventory;
using ShakeAPI.Inventory.Meta;
using ShakeAPI.Materials;
using ShakeAPI.Util;
using System;
using System.Collections.Generic;

namespace ShakeAPI.Inventory
{
    public class ItemStack : ICloneable
    {
        private Material type = Material.Air;

        public short Durability { get; set; }

        public int Amount { get; set; }

        public IItemMeta Meta { get; set; }

        public MaterialData Data { get; set; }

        public Material Type
        {
            [Utility]
            get => type;

            [Utility]
            set
            {
                Validate.NotNull(value, "Material cannot be null");

            }
        }

        [Utility]
        protected ItemStack()
        {
            // ;
        }

#pragma warning disable 0618
        public ItemStack(Material type, int amount = 1, short damage = 0) : this(type, amount, damage, null)
        {
            // ;
        }
        [Obsolete("This method uses an ambiguous data byte? object.")]
        public ItemStack(Material type, int amount, short damage, byte? data)
        {
            Validate.NotNull(type, "Material cannot be null.");

            Type = type;
            Amount = amount;
            if (damage != 0)
            {
                Durability = damage;
            }
            if (data != null)
            {
                CreateData((byte)data);
            }
        }
#pragma warning restore 0618

        public ItemStack(ItemStack stack)
        {
            Validate.NotNull(stack, "Cannot copy null stack.");
            Type = stack.Type;
            Amount = stack.Amount;
            Durability = stack.Durability;
            Data = stack.Data;
        }

        public int GetMaxStackSize()
        {
            Material material = Type;
            return material.MaxStack;
        }

        private void CreateData(byte data)
        {
            // TODO: Data = Type.GetNewData(data);
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }

    public interface IInventory : ICollection<ItemStack>
    {
        int Size { get; }
        int MaxStackSize { get; set; }
        string Name { get; }

        ItemStack GetItem(int index);
        void SetItem(int index, ItemStack item);

        /// <summary>
        /// 用以取代 <see cref="GetItem(int)"/> 及 <see cref="SetItem(int, ItemStack)"/>。
        /// </summary>
        /// <param name="index">物品欄 (slot) 索引。</param>
        /// <returns>存放於該物品欄中的 <see cref="ItemStack"/>。</returns>
        ItemStack this[int index] { get; set; }

        /// <summary>
        /// Stores the given ItemStacks in the inventory. This will try to fill 
        /// existing stacks and empty slots as well as it can.
        /// </summary>
        /// <exception cref="ArgumentException">Throws if items or any element in it is null.</exception>
        /// <param name="items"></param>
        /// <returns>A <see cref="Dictionary{int, ItemStack}"/> containing items that didn't fit.</returns>
        Dictionary<int, ItemStack> AddItem(params ItemStack[] items);

        /// <summary>
        /// Removes the given ItemStacks from the inventory.
        /// It will try to remove 'as much as possible' from the types and amounts you give as arguments.
        /// </summary>
        /// <exception cref="ArgumentException">Throws if items or any element in it is null.</exception>
        /// <param name="items"></param>
        /// <returns>A <see cref="Dictionary{int, ItemStack}"/> containing items that couldn't be removed.</returns>
        Dictionary<int, ItemStack> RemoveItem(params ItemStack[] items);

        ItemStack[] GetContents();
        void SetContents(ItemStack[] items);
        [Obsolete]
        bool Contains(int materialId);

        /// <summary>
        /// Checks if the inventory contains any <see cref="ItemStack"/> with the given material.
        /// </summary>
        /// <exception cref="ArgumentException">Throws if material is null.</exception>
        /// <param name="material">The material to check for.</param>
        /// <returns>True if an <see cref="ItemStack"/> is found with the given <see cref="Material"/>.</returns>
        bool Contains(Material material);

        new bool Contains(ItemStack item);

        [Obsolete]
        bool Contains(int materialId, int amount);
        bool Contains(Material material, int amount);
        bool Contains(ItemStack item, int amount);

        bool ContainsAtLeast(ItemStack item, int amount);

        Dictionary<int, ItemStack> All(Material material);
        Dictionary<int, ItemStack> All(ItemStack item);
        [Obsolete]
        int First(int materialId);
        int First(Material material);
        int First(ItemStack item);
        int FirstEmpty();
        [Obsolete]
        void Remove(int materialId);
        void Remove(Material material);
        new void Remove(ItemStack item);
        void Clear(int index);
        new void Clear();

        List<IHumanEntity> GetViewers();
        string Title { get; }
        InventoryType Type { get; }
        IInventoryHolder Holder { get; }
    }

    public interface IInventoryHolder
    {
        /// <summary>
        /// 該物件的背包。
        /// </summary>
        IInventory Inventory { get; }
    }
}
