﻿using ShakeAPI.Enchantments;
using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI.Inventory.Meta
{
    public interface IItemMeta : ICloneableG<IItemMeta> // , IConfigurationSerializable
    {
        bool HasDisplayName();
        string DisplayName { get; set; }
        bool HasLore();
        List<string> Lore { get; set; }
        bool HasEnchants();
        bool HasEnchant(Enchantment ench);
    }
}
