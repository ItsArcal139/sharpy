﻿using System;

using ShakeAPI.Configuration.Serialization;
using ShakeAPI.Entity;
using ShakeAPI.Permissions;
using ShakeAPI.Util;

namespace ShakeAPI
{
    public interface IOfflinePlayer : IServerOperator, IAnimalTamer, IConfigurationSerializable
    {
        bool IsOnline { get; }
        string Name { get; }
        Uuid UniqueId { get; }
        bool IsBanned { get; }
        bool IsWhitelisted { get; set; }
        [Nullable]
        IPlayer Player { get; }
        long FirstPlayed { get; }
        bool HasPlayedBefore { get; }
        Location BedSpawnLocation { get; }
    }
}