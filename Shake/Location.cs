﻿using ShakeAPI.Blocks;
using ShakeAPI.Entity;
using ShakeAPI.Inventory;
using ShakeAPI.Metadata;
using ShakeAPI.Plugins.Messaging;
using ShakeAPI.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI
{
    public class Location
    {
        public IWorld World { get; set; }
    }

    public interface IWorld : IPluginMessageRecipient, IMetadatable
    {
        [NotNull]
        IBlock GetBlockAt(int x, int y, int z);
        [NotNull]
        IBlock GetBlockAt(Location location);
        int GetHighestBlockYAt(int x, int z);
        int GetHighestBlockYAt(Location location);
        [NotNull]
        IBlock GetHighestBlockAt(int x, int z);
        [NotNull]
        IBlock GetHighestBlockAt(Location location);
        [NotNull]
        IChunk GetChunkAt(int x, int z);
        [NotNull]
        IChunk GetChunkAt(Location location);
        [NotNull]
        IChunk GetChunkAt(IBlock block);
        bool IsChunkLoaded(IChunk chunk);
        [NotNull]
        IChunk[] GetLoadedChunks();
        void LoadChunk(IChunk chunk);
        bool IsChunkLoaded(int x, int z);
        bool IsChunkGenerated(int x, int z);
        bool IsChunkInUse(int x, int z);
        void LoadChunk(int x, int z);
        bool LoadChunk(int x, int z, bool generate);
        bool UnloadChunk(IChunk chunk);
        bool UnloadChunk(int x, int z);
        bool UnloadChunk(int x, int z, bool save);
        [Obsolete("It's never safe to remove a chunk in use.")]
        bool UnloadChunk(int x, int z, bool save, bool safe);
        bool UnloadChunkRequest(int x, int z);
        bool UnloadChunkRequest(int x, int z, bool safe);
        bool RegenerateChunk(int x, int z);
        bool RefreshChunk(int x, int z);
        bool IsChunkForceLoaded(int x, int z);
        void SetChunkForceLoaded(int x, int z, bool forced);
        ICollection<IChunk> GetForceLoadedChunks();
        IItem DropItem(Location location, ItemStack item);
        // TODO: More....
    }

    public interface IChunk
    {
    }
}
