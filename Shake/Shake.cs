﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShakeAPI.Entity;
using ShakeAPI.Schedulers;
using ShakeAPI.Util;

namespace ShakeAPI
{
    public static class Shake
    {
        private static IServer server = null;

        public static IServer Server
        {
            [NotNull]
            get => server;

            set
            {
                if(server != null)
                {
                    throw new InvalidOperationException("Cannot redefine singleton Server.");
                }

                server = value;
                server.Logger.Info($"This server is running {Name} version {Version} (implementing API version {ShakeVersion})");
            }
        }

        [NotNull]
        public static string Name => Server.Name;

        [NotNull]
        public static string Version => Server.Version;

        [NotNull]
        public static string ShakeVersion => Server.ShakeVersion;

        [NotNull]
        public static ICollection<T> GetOnlinePlayers<T>() where T : IPlayer => Server.GetOnlinePlayers<T>();

        public static int MaxPlayers => Server.MaxPlayers;

        public static int Port => Server.Port;

        public static int ViewDistance => Server.ViewDistance;

        public static IShakeScheduler Scheduler => Server.Scheduler;

        public static ITag<T> GetTag<T>(string registry, NamespacedKey key, GenericType<T> clazz) where T : IKeyed => Server.GetTag(registry, key, clazz);
    }
}