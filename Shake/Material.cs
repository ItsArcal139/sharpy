﻿using ShakeAPI.Blocks.Data;
using ShakeAPI.Blocks.Data.Types;
using ShakeAPI.Materials;
using ShakeAPI.Util;
using System;
using System.Reflection;

namespace ShakeAPI
{
    public class Material : IKeyed
    {
        #region => A
        public static readonly Material AcaciaBoat = new Material("AcaciaBoat", 27326, 1);
        public static readonly Material AcaciaButton = new Material("AcaciaButton", 13993, typeof(ISwitch));
        public static readonly Material AcaciaDoor = new Material("AcaciaDoor", 23797, typeof(IDoor));
        public static readonly Material AcaciaFence = new Material("AcaciaFence", 4569, typeof(IFence));
        public static readonly Material AcaciaFenceGate = new Material("AcaciaFenceGate", 14145, typeof(IGate));
        public static readonly Material AcaciaLeaves = new Material("AcaciaLeaves", 16606, typeof(ILeaves));
        public static readonly Material AcaciaLog = new Material("AcaciaLog", 8385, typeof(IOrientable));
        public static readonly Material AcaciaPlanks = new Material("AcaciaPlanks", 31312);
        public static readonly Material AcaciaPressurePlate = new Material("AcaciaPressurePlate", 17586, typeof(IPowerable));
        public static readonly Material AcaciaSaping = new Material("AcaciaSaping", 20806, typeof(ISapling));

        public static readonly Material AcaciaSign = new Material("AcaciaSign", 29808, 16, typeof(ISign));

        public static readonly Material AcaciaSlab = new Material("AcaciaSlab", 23730, typeof(ISlab));
        public static readonly Material AcaciaStairs = new Material("AcaciaStairs", 17453, typeof(IStairs));
        public static readonly Material AcaciaTrapdoor = new Material("AcaciaTrapdoor", 18343, typeof(ITrapDoor));
        public static readonly Material AcaciaWallSign = new Material("AcaciaWallSign", 20316, 16, typeof(IWallSign));
        public static readonly Material AcaciaWood = new Material("AcaciaWood", 9451, typeof(IOrientable));
        public static readonly Material ActivatorRail = new Material("ActivatorRail", 5834, typeof(IRedstoneRail));
        public static readonly Material Air = new Material("Air", 9648);
        public static readonly Material Allium = new Material("Allium", 6871);
        public static readonly Material Andesite = new Material("Andesite", 25975);

        public static readonly Material AndesiteSlab = new Material("AndesiteSlab", 32124, typeof(ISlab));
        public static readonly Material AndesiteStairs = new Material("AndesiteStairs", 17747, typeof(IStairs));
        public static readonly Material AndesiteWall = new Material("AndesiteWall", 14938, typeof(IFence));

        public static readonly Material Anvil = new Material("Anvil", 18718);
        public static readonly Material Apple = new Material("Apple", 7720);
        public static readonly Material ArmorStand = new Material("ArmorStand", 12852);
        public static readonly Material Arrow = new Material("Arrow", 31091);
        public static readonly Material AttachedMelonStem = new Material("AttachedMelonStem", 30882, typeof(IDirectional));
        public static readonly Material AttachedPumpkinStem = new Material("AttachedPumpkinStem", 12724, typeof(IDirectional));
        public static readonly Material AzureBluet = new Material("AzureBluet", 17608);
        #endregion

        #region => B
        public static readonly Material BakedPotato = new Material("BakedPotato", 14624);

        public static readonly Material Bamboo = new Material("Bamboo", 18728, typeof(IBamboo));
        public static readonly Material BambooSapling = new Material("BambooSapling", 8478);
        public static readonly Material Barrel = new Material("Barrel", 22396, typeof(IDirectional));

        public static readonly Material Barrier = new Material("Barrier", 26453);
        public static readonly Material BatSpawnEgg = new Material("BatSpawnEgg", 14607);
        public static readonly Material Beacon = new Material("Beacon", 6608);
        public static readonly Material Bedrock = new Material("Bedrock", 23130);
        public static readonly Material Beef = new Material("Beef", 4803);
        public static readonly Material Beetroot = new Material("Beetroot", 23305);
        public static readonly Material Beetroots = new Material("Beetroots", 22075, typeof(IAgeable));
        public static readonly Material BeetrootSeeds = new Material("BeetrootSeeds", 21282);
        public static readonly Material BeetrootSoup = new Material("BeetrootSoup", 22075, 1);

        public static readonly Material Bell = new Material("Bell", 20000, typeof(IBell));

        public static readonly Material BirchBoat = new Material("BirchBoat", 28104, 1);
        public static readonly Material BirchButton = new Material("BirchButton", 26934, typeof(ISwitch));
        public static readonly Material BirchDoor = new Material("BirchDoor", 14759, typeof(IDoor));
        public static readonly Material BirchFence = new Material("BirchFence", 17347, typeof(IFence));
        public static readonly Material BirchFenceGate = new Material("BirchFenceGate", 6322, typeof(IGate));
        public static readonly Material BirchLeaves = new Material("BirchLeaves", 12601, typeof(ILeaves));
        public static readonly Material BirchLog = new Material("BirchLog", 26727, typeof(IOrientable));
        public static readonly Material BirchPlanks = new Material("BirchPlanks", 29322);
        public static readonly Material BirchPressurePlate = new Material("BirchPressurePlate", 9664, typeof(IPowerable));
        public static readonly Material BirchSaping = new Material("BirchSaping", 31533, typeof(ISapling));
        public static readonly Material BirchSign = new Material("BirchSign", 11351, 16, typeof(ISign));
        public static readonly Material BirchSlab = new Material("BirchSlab", 13807, typeof(ISlab));
        public static readonly Material BirchStairs = new Material("BirchStairs", 7657, typeof(IStairs));
        public static readonly Material BirchTrapdoor = new Material("BirchTrapdoor", 32585, typeof(ITrapDoor));
        public static readonly Material BirchWallSign = new Material("BirchWallSign", 9887, 16, typeof(IWallSign));
        public static readonly Material BirchWood = new Material("BirchWood", 20913, typeof(IOrientable));

        public static readonly Material BlackBanner = new Material("BlackBanner", 9365, 16, typeof(IRotatable));
        public static readonly Material BlackBed = new Material("BlackBed", 20490, 1, typeof(IBed));
        public static readonly Material BlackCarpet = new Material("BlackCarpet", 6056);
        public static readonly Material BlackConcrete = new Material("BlackConcrete", 13338);
        public static readonly Material BlackConcretePowder = new Material("BlackConcretePowder", 16150);
        public static readonly Material BlackDye = new Material("BlackDye", 6202);
        public static readonly Material BlackGlazedTerracotta = new Material("BlackGlazedTerracotta", 29678, typeof(IDirectional));

        #endregion

        // TODO: https://hub.spigotmc.org/stash/projects/SPIGOT/repos/bukkit/browse/src/main/java/org/bukkit/Material.java#278
        // N lines more to do below...

        // == Legacy Materials below ==
        #region => Legacy definitions.
        [Obsolete] public static readonly Material LegacyAir = new Material("LegacyAir", 0);
        [Obsolete] public static readonly Material LegacyStone;
        [Obsolete] public static readonly Material LegacyGrass;
        [Obsolete] public static readonly Material LegacyDirt;
        [Obsolete] public static readonly Material LegacyCobblestone;
        [Obsolete] public static readonly Material LegacyWood;
        [Obsolete] public static readonly Material LegacySapling;
        [Obsolete] public static readonly Material LegacyBedrock;
        [Obsolete] public static readonly Material LegacyWater;
        [Obsolete] public static readonly Material LegacyStationaryWater;
        [Obsolete] public static readonly Material LegacyLava;
        [Obsolete] public static readonly Material LegacyStationaryLava;
        [Obsolete] public static readonly Material LegacySand;
        [Obsolete] public static readonly Material LegacyGravel;
        [Obsolete] public static readonly Material LegacyGoldOre;
        [Obsolete] public static readonly Material LegacyIronOre;
        [Obsolete] public static readonly Material LegacyCoalOre;
        [Obsolete] public static readonly Material LegacyLog;
        [Obsolete] public static readonly Material LegacyLeaves;
        [Obsolete] public static readonly Material LegacySponge;
        [Obsolete] public static readonly Material LegacyGlass;
        [Obsolete] public static readonly Material LegacyLapisOre;
        [Obsolete] public static readonly Material LegacyLapisBlock;
        [Obsolete] public static readonly Material LegacyDispenser;
        [Obsolete] public static readonly Material LegacySandstone;
        [Obsolete] public static readonly Material LegacyNoteBlock;
        [Obsolete] public static readonly Material LegacyBedBlock;
        [Obsolete] public static readonly Material LegacyPoweredRail;


        [Obsolete] public static readonly Material LegacyLeatherBoots;
        [Obsolete] public static readonly Material LegacyChainmailBoots;
        [Obsolete] public static readonly Material LegacyIronBoots;
        [Obsolete] public static readonly Material LegacyDiamondBoots;
        [Obsolete] public static readonly Material LegacyGoldBoots;
        [Obsolete] public static readonly Material LegacyLeatherLeggings;
        [Obsolete] public static readonly Material LegacyChainmailLeggings;
        [Obsolete] public static readonly Material LegacyIronLeggings;
        [Obsolete] public static readonly Material LegacyDiamondLeggings;
        [Obsolete] public static readonly Material LegacyGoldLeggings;
        [Obsolete] public static readonly Material LegacyLeatherChestplate;
        [Obsolete] public static readonly Material LegacyChainmailChestplate;
        [Obsolete] public static readonly Material LegacyIronChestplate;
        [Obsolete] public static readonly Material LegacyDiamondChestplate;
        [Obsolete] public static readonly Material LegacyGoldChestplate;
        [Obsolete] public static readonly Material LegacyLeatherHelmet;
        [Obsolete] public static readonly Material LegacyChainmailHelmet;
        [Obsolete] public static readonly Material LegacyIronHelmet;
        [Obsolete] public static readonly Material LegacyDiamondHelmet;
        [Obsolete] public static readonly Material LegacyGoldHelmet;
        [Obsolete] public static readonly Material LegacyWoodenSword;
        [Obsolete] public static readonly Material LegacyStoneSword;
        [Obsolete] public static readonly Material LegacyIronSword;
        [Obsolete] public static readonly Material LegacyDiamondSword;
        [Obsolete] public static readonly Material LegacyGoldSword;
        [Obsolete] public static readonly Material LegacyWoodenShovel;
        [Obsolete] public static readonly Material LegacyStoneShovel;
        [Obsolete] public static readonly Material LegacyIronShovel;
        [Obsolete] public static readonly Material LegacyDiamondShovel;
        [Obsolete] public static readonly Material LegacyGoldShovel;
        [Obsolete] public static readonly Material LegacyWoodenHoe;
        [Obsolete] public static readonly Material LegacyStoneHoe;
        [Obsolete] public static readonly Material LegacyIronHoe;
        [Obsolete] public static readonly Material LegacyDiamondHoe;
        [Obsolete] public static readonly Material LegacyGoldHoe;
        [Obsolete] public static readonly Material LegacyWoodenAxe;
        [Obsolete] public static readonly Material LegacyStoneAxe;
        [Obsolete] public static readonly Material LegacyIronAxe;
        [Obsolete] public static readonly Material LegacyDiamondAxe;
        [Obsolete] public static readonly Material LegacyGoldAxe;
        [Obsolete] public static readonly Material LegacyWoodenPickaxe;
        [Obsolete] public static readonly Material LegacyStonePickaxe;
        [Obsolete] public static readonly Material LegacyIronPickaxe;
        [Obsolete] public static readonly Material LegacyDiamondPickaxe;
        [Obsolete] public static readonly Material LegacyGoldPickaxe;
        [Obsolete] public static readonly Material LegacyShears;
        [Obsolete] public static readonly Material LegacyFlintAndSteel;
        [Obsolete] public static readonly Material LegacyBow;
        [Obsolete] public static readonly Material LegacyFishingRod;

        [Obsolete] public static readonly Material LegacyRecord_3 = new Material("LegacyRecord_3", 2258, 1);
        [Obsolete] public static readonly Material LegacyRecord_4 = new Material("LegacyRecord_4", 2259, 1);
        [Obsolete] public static readonly Material LegacyRecord_5 = new Material("LegacyRecord_5", 2260, 1);
        [Obsolete] public static readonly Material LegacyRecord_6 = new Material("LegacyRecord_6", 2261, 1);
        [Obsolete] public static readonly Material LegacyRecord_7 = new Material("LegacyRecord_7", 2262, 1);
        [Obsolete] public static readonly Material LegacyRecord_8 = new Material("LegacyRecord_8", 2263, 1);
        [Obsolete] public static readonly Material LegacyRecord_9 = new Material("LegacyRecord_9", 2264, 1);
        [Obsolete] public static readonly Material LegacyRecord_10 = new Material("LegacyRecord_10", 2265, 1);
        [Obsolete] public static readonly Material LegacyRecord_11 = new Material("LegacyRecord_11", 2266, 1);
        [Obsolete] public static readonly Material LegacyRecord_12 = new Material("LegacyRecord_12", 2267, 1);
        #endregion

        private readonly Type md = typeof(MaterialData);

        public const string LegacyPrefix = "Legacy";

        public static Material GetMaterial(string name)
        {
            // TODO: return GetMaterial(name, false);
            return null;
        }

        /// <summary>
        /// 物品 ID。無論如何，請別再使用這個屬性。
        /// </summary>
        [Obsolete]
        public int Id { get; private set; }
        public ConstructorInfo Constructor { get; private set; }
        public int MaxStack { get; private set; }
        public short Durability { get; private set; }
        public Type Data { get; private set; }

        /// <summary>
        /// 是否為 1.13 之前的插件使用的屬性。無論如何，請別再使用這個屬性。
        /// </summary>
        [Obsolete]
        public bool Legacy { get; private set; }

        private NamespacedKey key;
        public NamespacedKey Key => _GetKey0();

        private Material(string name, int id, int stack = 64, short durability = 0) : this(name, id, stack, durability, typeof(MaterialData)) { }

        private Material(string name, int id, Type data) : this(name, id, 64, data) { }

        private Material(string name, int id, int stack, Type data) : this(name, id, stack, 0, data) { }

#pragma warning disable 0612
        private Material(string name, int id, int stack, short durability, Type data)
        {
            Id = id;
            Durability = durability;
            MaxStack = stack;
            Data = data;
            Legacy = name.StartsWith(LegacyPrefix);
            key = NamespacedKey.MinecraftKey(StringConvert.ConvertToUnderlineCase(name));

            if (data.IsExtendOf(typeof(MaterialData)))
            {
                ConstructorInfo info = data.GetConstructor(new Type[] { typeof(Material), typeof(byte) });
                Validate.AssertNull<NoSuchMethodException>(info, $"No constructor found in type {data.Name}.");

                Constructor = info;
            }
            else
            {
                Constructor = null;
            }
        }

        private NamespacedKey _GetKey0()
        {
            Validate.IsTrue(Legacy, "Cannot get key of legacy Material.");
            return key;
        }
#pragma warning restore 0612

        public IBlockData CreateBlockData()
        {
            return null; // TODO: Shake.CreateBlockData(this);
        }

        public bool IsBlock => _IsBlock();

        private bool _IsBlock()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsEdible => _IsEdible();

        private bool _IsEdible()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsRecord => _IsRecord();

        private bool _IsRecord()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsSolid => _IsSolid();

        private bool _IsSolid()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsTransparent => _IsTransparent();

        private bool _IsTransparent()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsFlammable => _IsFlammable();

        private bool _IsFlammable()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsBurnable => _IsBurnable();

        private bool _IsBurnable()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsFuel => _IsFuel();

        private bool _IsFuel()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsOccluding => _IsOccluding();

        private bool _IsOccluding()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool HasGravity => _HasGravity();

        private bool _HasGravity()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool IsItem => _IsItem();

        private bool _IsItem()
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public bool Interactable => _IsInteractable(this);

        private static bool _IsInteractable(Material m)
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public float Hardness => _GetHardness(this);

        private static float _GetHardness(Material m)
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }

        public float BlastResistance => _GetBlastResistance(this);

        private static float _GetBlastResistance(Material m)
        {
            // TODO: The huge implementation.
            throw new NotImplementedException();
        }
    }
}
