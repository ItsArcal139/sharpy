﻿namespace ShakeAPI.Commands
{
    public interface ICommandSender
    {
        void SendMessage(string message);
    }
}
