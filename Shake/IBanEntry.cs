﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShakeAPI
{
    public interface IBanEntry
    {
        string Target { get; }
        DateTime Created { get; set; }
        string Source { get; set; }
        DateTime Expiration { get; set; }
        string Reason { get; set; }

        void Save();
    }
}
