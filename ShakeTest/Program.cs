﻿using ShakeAPI;
using ShakeAPI.Util;
using System;

namespace ShakeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // Output: underline_case3_gogo
            Console.WriteLine(StringConvert.ConvertToUnderlineCase("UnderlineCase3Gogo"));

            // Output: minecraft:birch_button
            Material m = Material.BirchButton;
            Console.WriteLine(m.Key.ToString());

            Console.ReadLine();
        }
    }
}
